#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <LedControl.h>


LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);      //LCD ekran

LedControl led = LedControl(8, 11, 10, 4);  // LED matrix


int lijevo = 6;
int desno = 7;
int sredina = 4;
int gore = 3;
int dolje = 2;      //deklaracija varijabli za push buttone


int tezina;
unsigned int pomak = 0;           //pomak kursora LCD ekrana
boolean tezinaflag = true;        //flag koji kontrolira loop pri odabiru težine
unsigned int ledpomakx = 1;       //ova tri pomaka koriste se za kontrolu LED matria
unsigned int ledpomaky = 1;
unsigned int ledpomakdis = 0;
boolean igraflag = false;         //flag koji kontrolira loop pri pokretanju igre
boolean bling = true;             //flag koji kontrolira blinkanje kartica na LED matrixu
unsigned int brojdisplaya;        //broj displaya koji se koristi ovisno o težini igre
int kartice[16];            //polje u koje se spremaju vrijednosti 0-9 koje označavaju pojedinu karticu
int odabranakartica1, odabranakartica2;              //redni broj kartice u matrixu
int picindex1, picindex2;                     //indeks slike koja se nalazi na kartici
boolean kartica1 = false, kartica2 = false; //flag koji pokazuje je li kartica otvorena
int rezultat = 0;
int koordinate[17][3];                          //matrica 17x3 u koju se spremaju koordinate (dis,x,y)- [0][x] je za trenutno otvorenu karticu prilikom odabira druge, a preostalih 16 za pogodjene kartice
int brojac = 0;                             //pomocni brojac za indekse polja pogodjeno[]
boolean karticaflag = false;
int temp[3];                  //pomoćne koordinate za provjeru dostupnosti pojedine koordinate na koju treba staviti pokazivač
int koordinateindex[16];      //spremljene koordinate... otvorena prva je odabranakartica1
int koordindex;

void setup() {
  lcd.begin(16, 2);              // potrebno da bismo mogli koristiti LCD.
  lcd.backlight();              //pozadinsko svjetlo za LCD
  
  pinMode(lijevo, INPUT);
  pinMode(desno, INPUT);
  pinMode(gore, INPUT);
  pinMode(dolje, INPUT);
  pinMode(sredina, INPUT);      //push buttoni su ulaz

  lcd.setCursor(0, 0);          //namještanje LCD ekrana za početni izbornik
  lcd.print("Broj kartica:");
  lcd.setCursor(0, 1);
  lcd.print("4  8  12 16");
  lcd.setCursor(0, 1);
  lcd.cursor();
  lcd.blink();
  delay(2000);

  led.shutdown(0, false);       //setup LED matrixa
  led.setIntensity(0, 1);
  led.clearDisplay(0);
  led.shutdown(1, false);
  led.setIntensity(1, 1);
  led.clearDisplay(1);
  led.shutdown(2, false);
  led.setIntensity(2, 1);
  led.clearDisplay(2);
  led.shutdown(3, false);
  led.setIntensity(3, 1);
  led.clearDisplay(3);

  randomSeed(analogRead(0));        //generiranje random seeda za odabir parova
}




void loop() {
  if (tezinaflag == true) {
    if (digitalRead(desno) == true && pomak != 9) {    //pomak kursora LCD displaya pri odabiru težine
      pomak = pomak + 3;
      lcd.setCursor(pomak, 1);
      delay(500);
    }
    if (digitalRead(lijevo) == true && pomak != 0) {  //pomak kursora LCD displaya pri odabiru težine
      pomak = pomak - 3;
      lcd.setCursor(pomak, 1);
      delay(500);
    }
    if (digitalRead(sredina) == true) { //kontrola srednjeg buttona za odabir željene težine
      PostaviIgru();
      tezinaflag = false;   //pauzira se loop kako se ne bi stalno vrtio dok se vrti funkcija pokreniigru()
    }
  } //kraj tezina flaga

  if (igraflag == true) {

    //blinkanje odabrane kartice LED matrixa
    if (bling == true) {
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, false);
      bling = false;
      delay(200);
    }
    if (bling == false) {
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      bling = true;
      delay(200);   //izmjenjuje se paljenje gašenje svakih 200ms
    }

    //////////////////////////pomicanje buttonima po matrixima/////////////////////////////////////////

    ///////////////grananje za određivanje indexa koordinate pomaka////////////
    if (ledpomakdis == 0) {
      if (ledpomakx == 1) {
        if (ledpomaky == 1) {
          koordindex = 0;
        }
        if (ledpomaky == 5) {
          koordindex = 1;
        }
      }

      if (ledpomakx == 5) {
        if (ledpomaky == 1) {
          koordindex = 2;
        }
        if (ledpomaky == 5) {
          koordindex = 3;
        }
      }
    }

    if (ledpomakdis == 1) {
      if (ledpomakx == 1) {
        if (ledpomaky == 1) {
          koordindex = 4;
        }
        if (ledpomaky == 5) {
          koordindex = 5;
        }
      }

      if (ledpomakx == 5) {
        if (ledpomaky == 1) {
          koordindex = 6;
        }
        if (ledpomaky == 5) {
          koordindex = 7;
        }
      }
    }

    if (ledpomakdis == 2) {
      if (ledpomakx == 1) {
        if (ledpomaky == 1) {
          koordindex = 8;
        }
        if (ledpomaky == 5) {
          koordindex = 9;
        }
      }

      if (ledpomakx == 5) {
        if (ledpomaky == 1) {
          koordindex = 10;
        }
        if (ledpomaky == 5) {
          koordindex = 11;
        }
      }
    }

    if (ledpomakdis == 3) {
      if (ledpomakx == 1) {
        if (ledpomaky == 1) {
          koordindex = 12;
        }
        if (ledpomaky == 5) {
          koordindex = 13;
        }
      }

      if (ledpomakx == 5) {
        if (ledpomaky == 1) {
          koordindex = 14;
        }
        if (ledpomaky == 5) {
          koordindex = 15;
        }
      }
    }





    ///////////gore////////////
    if (digitalRead(gore) == true && ledpomaky == 5) {
      for (int i = 0; i < brojac + 1; i++) {
        if (ledpomakdis == koordinate[i][0] && ledpomakx == koordinate[i][1] && koordinate[i][2] == 1) {  //ako je iznad pokaznika otvorena/pogodjena kartica
          karticaflag = true;
          break;
        }
      }
      if (karticaflag == false) {       //ako nije pomakne se gore
        ledpomaky = 1;
      }

      else {
        int temp = koordindex + 1;
        for (int i = 0; i < brojac; i++) {
          if (temp == koordinateindex[i]) {
            temp=temp+2;
          }
        }
        if (temp<(brojdisplaya+1)*4) {
          Pokaznik(temp);
        }
        else {
          temp=(brojdisplaya+1)*4-2;
          for (int i=brojac-1; i>=0; i--) {
            if (temp==koordinateindex[i]) {
              temp=temp-2;
            }
          }
          if (temp>=0) {
            Pokaznik(temp);
          }
        }
      }


      karticaflag = false;
    }

    /////////dolje/////////////
    if (digitalRead(dolje) == true && ledpomaky == 1) {
      for (int i = 0; i < brojac + 1; i++) {
        if (ledpomakdis == koordinate[i][0] && ledpomakx == koordinate[i][1] && koordinate[i][2] == 5) {    //ako je ispod pokaznika otvorena/pogodjena kartica
          karticaflag = true;
          break;
        }
      }
      if (karticaflag == false) {
        ledpomaky = 5;              //ako nije pomakne se gore
      }

      else {
        int temp = koordindex - 1;
        for (int i = brojac-1; i >= 0; i--) {
          if (temp == koordinateindex[i]) {
            temp=temp-2;
          }
        }
        if (temp>=0) {
          Pokaznik(temp);
        }
        else {
          temp=1;
          for (int i = 0; i < brojac; i++) {
            if (temp == koordinateindex[i]) {
              temp=temp+2;
          }
        }
        if (temp<(brojdisplaya+1)*4) {
          Pokaznik(temp);
        }
       }
     }


      karticaflag = false;
    }

    ///////////////desno//////////////
    if (digitalRead(desno) == true) {
      if (ledpomakx == 1) {             //ako je pokaznik na lijevoj strani matrixa, odnosno ako pomakom udesno ne mora prelaziti na sljedeći matrix
        for (int i = 0; i < brojac + 1; i++) {
          if (ledpomakdis == koordinate[i][0] && ledpomaky == koordinate[i][2]) {         //ako je desno od pokaznika otvorena/pogodjena kartica
            karticaflag = true;
            break;
          }
        }
        if (karticaflag == false ) {                //ako nije, pomakne se desno
          ledpomakx = 5;
        }
        else if (ledpomakdis < brojdisplaya) {
          int temp = koordindex + 2;
          
          for (int i = 0; i < brojac+1; i++) {
            if (temp == koordinateindex[i] || temp == odabranakartica1) {
              temp=temp+2;
            }
          }
          if (temp<(brojdisplaya+1)*4) {
            Pokaznik(temp);
          }
        }
        
        karticaflag = false;
      }

      else if (ledpomakx == 5  && ledpomakdis < brojdisplaya) {             //ako je pokaznik na desnoj strani matrixa, odnosno ako pomakom udesno mora prelaziti na sljedeći matrix
        for (int i = 0; i < brojac + 1; i++) {
          if ((ledpomakdis + 1) == koordinate[i][0] && ledpomaky == koordinate[i][2]) {     //ako je desnoo od pokaznika otvorena/pogodjena kartica
            karticaflag = true;
            break;
          }
        }
        if (karticaflag == false) {           //ako nije, pomakne se na prvo desno polje, odnosno na sljedeći matrix
          ledpomakdis++;
          ledpomakx = 1;
        }
        else {
          int temp = koordindex + 2;
          for (int i = 0; i < brojac+1; i++) {
            if (temp == koordinateindex[i] || temp == odabranakartica1) {
              temp=temp+2;
            }
          }
        if (temp<(brojdisplaya+1)*4) {
          Pokaznik(temp);
        }
        }
        karticaflag = false;
      }
    }

    ///////////lijevo///////////////
    if (digitalRead(lijevo) == true) {
      if (ledpomakx == 5) {
        for (int i = 0; i < brojac + 1; i++) {
          if (ledpomakdis == koordinate[i][0] && ledpomaky == koordinate[i][2]) {
            karticaflag = true;
            break;
          }
        }
        if (karticaflag == false ) {
          ledpomakx = 1;
        }
        else if (ledpomakdis > 0) {
          int temp = koordindex - 2;
          for (int i = brojac; i >= 0; i--) {
            if (temp == koordinateindex[i] || temp == odabranakartica1) {
              temp=temp-2;
            }
          }
          if (temp>=0) {
             Pokaznik(temp);
          }
       }
       karticaflag = false;
     }
      
     

      else if (ledpomakx == 1  && ledpomakdis > 0) {
        for (int i = 0; i < brojac + 1; i++) {
          if ((ledpomakdis - 1) == koordinate[i][0] && ledpomaky == koordinate[i][2]) {
            karticaflag = true;
            break;
          }
        }
        if (karticaflag == false) {
          ledpomakdis--;
          ledpomakx = 5;
        }
        else {
          int temp = koordindex - 2;
          for (int i = brojac; i >= 0; i--) {
            if (temp == koordinateindex[i] ||temp == odabranakartica1) {
              temp=temp-2;
            }
          }
          if (temp>=0) {
            Pokaznik(temp);
          }
        }
        karticaflag = false;
      }
    }


    ///////////////////////odabir kartice srednjim buttonom////////////////////////////////////////
    if (digitalRead(sredina) == true) {
      Sort();
      if (kartica1 == false && kartica2 == false) {
        OdabirKartice1();
      }
      else if (kartica1 == true and kartica2 == false) {
        OdabirKartice2();
      }
    }


  } //kraj igraflaga


} //kraj loopa



void PostaviIgru() {
  //odabir tezine
  lcd.clear();   //ispis rezultata na LCD ekranu
  lcd.setCursor(0, 0);
  lcd.noCursor();
  lcd.noBlink();
  lcd.print("Tezina: ");


  switch (pomak) {    //ovisno o odabranoj težini definira se broj matrixa koji će se koristiti te se postavljaju kartice (pale se potrebne LEDice)
    case 0 :
      {
        tezina = 4;
        brojdisplaya = 0;
        led.setRow(0, 0, B00000000);
        led.setRow(0, 1, B01100110);
        led.setRow(0, 2, B01100110);
        led.setRow(0, 3, B00000000);
        led.setRow(0, 4, B00000000);
        led.setRow(0, 5, B01100110);
        led.setRow(0, 6, B01100110);
        led.setRow(0, 7, B00000000);

        break;
      }
    case 3 :
      {
        tezina = 8;
        brojdisplaya = 1;
        for (int i = 0; i < 2; i++) {
          led.setRow(i, 0, B00000000);
          led.setRow(i, 1, B01100110);
          led.setRow(i, 2, B01100110);
          led.setRow(i, 3, B00000000);
          led.setRow(i, 4, B00000000);
          led.setRow(i, 5, B01100110);
          led.setRow(i, 6, B01100110);
          led.setRow(i, 7, B00000000);
        }

        break;
      }
    case 6 :
      {
        tezina = 12;
        brojdisplaya = 2;
        for (int i = 0; i < 3; i++) {
          led.setRow(i, 0, B00000000);
          led.setRow(i, 1, B01100110);
          led.setRow(i, 2, B01100110);
          led.setRow(i, 3, B00000000);
          led.setRow(i, 4, B00000000);
          led.setRow(i, 5, B01100110);
          led.setRow(i, 6, B01100110);
          led.setRow(i, 7, B00000000);
        }

        break;
      }
    case 9 :
      {
        tezina = 16;
        brojdisplaya = 3;
        for (int i = 0; i < 4; i++) {
          led.setRow(i, 0, B00000000);
          led.setRow(i, 1, B01100110);
          led.setRow(i, 2, B01100110);
          led.setRow(i, 3, B00000000);
          led.setRow(i, 4, B00000000);
          led.setRow(i, 5, B01100110);
          led.setRow(i, 6, B01100110);
          led.setRow(i, 7, B00000000);
        }

        break;
      }


  }

  lcd.print(tezina);
  delay(2000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Rezultat: ");
  lcd.setCursor(0, 1);
  lcd.print(rezultat);


  //generiranje kartica pomoću random number generatora

  int lista2[10], lista1[10];

  for (int i = 0; i < 10; i++) {  //lista 0-9 koju ćemo poslije izmiješati
    lista1[i] = i;

  }


  for (int i = 0; i < tezina / 2; i++) { //generiranje liste kartica pomoću random generatora (odabire se samo potreban broj random kartica)
    int j = random(10);
    int temp = lista1[i];
    lista1[i] = lista1 [j];
    lista1 [j] = temp;
  }

  for (int i = 0; i < tezina / 2; i++) { //postavljanje druge liste s istim vrijednostima kao i prva (parovi)
    lista2[i] = lista1[i];
  }

  for (int i = 0; i < tezina / 2; i++) { //spajanje dvije liste u jednu
    kartice[i] = lista1[i];
    kartice[i + tezina / 2] = lista2[i];


  }

  for (int i = 0; i < tezina; i++) {    //miješanje kartica random generatorom po uzoru na Bubble Sort
    int j = random(i, tezina - 1);
    int temp = kartice[i];
    kartice[i] = kartice[j];
    kartice[j] = temp;
  }

 igraflag = true;    //flag za pokretanje upravo postavljene igre kako bi se loop mogao nastaviti


}

void OdabirKartice1() {
  
  //odabir kartice srednjim buttonom

  if (ledpomakdis == 0) {
    if (ledpomakx == 1) {
      if (ledpomaky == 1) {
        odabranakartica1 = 0;
      }
      if (ledpomaky == 5) {
        odabranakartica1 = 1;
      }
    }

    if (ledpomakx == 5) {
      if (ledpomaky == 1) {
        odabranakartica1 = 2;
      }
      if (ledpomaky == 5) {
        odabranakartica1 = 3;
      }
    }
  }

  if (ledpomakdis == 1) {
    if (ledpomakx == 1) {
      if (ledpomaky == 1) {
        odabranakartica1 = 4;
      }
      if (ledpomaky == 5) {
        odabranakartica1 = 5;
      }
    }

    if (ledpomakx == 5) {
      if (ledpomaky == 1) {
        odabranakartica1 = 6;
      }
      if (ledpomaky == 5) {
        odabranakartica1 = 7;
      }
    }
  }

  if (ledpomakdis == 2) {
    if (ledpomakx == 1) {
      if (ledpomaky == 1) {
        odabranakartica1 = 8;
      }
      if (ledpomaky == 5) {
        odabranakartica1 = 9;
      }
    }

    if (ledpomakx == 5) {
      if (ledpomaky == 1) {
        odabranakartica1 = 10;
      }
      if (ledpomaky == 5) {
        odabranakartica1 = 11;
      }
    }
  }

  if (ledpomakdis == 3) {
    if (ledpomakx == 1) {
      if (ledpomaky == 1) {
        odabranakartica1 = 12;
      }
      if (ledpomaky == 5) {
        odabranakartica1 = 13;
      }
    }

    if (ledpomakx == 5) {
      if (ledpomaky == 1) {
        odabranakartica1 = 14;
      }
      if (ledpomaky == 5) {
        odabranakartica1 = 15;
      }
    }
  }

  picindex1 = kartice[odabranakartica1];


  switch (picindex1) {
    case 0:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 1:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, false);
      break;

    case 2:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, false);
      break;

    case 3:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 4:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 5:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 6:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, false);
      break;

    case 7:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 8:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, false);
      break;

    case 9:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;
  }

  koordinate[0][0] = ledpomakdis;
  koordinate[0][1] = ledpomakx;
  koordinate[0][2] = ledpomaky;

////////povratak pokazivača na prvu slobodnu karticu
  Sort();
  int priv=koordindex;
    for (int i=0; i<brojac+1; i++) {
      if (priv == koordinateindex[i] || priv == odabranakartica1) {
        priv++;
     }
    }
    if (priv<=(brojdisplaya+1)*4-1) {
      Pokaznik(priv);
    }
    else {
      
      priv=(brojdisplaya+1)*4-1; 
      for (int i=brojac; i>=0; i--) {
          if (priv==koordinateindex[i] || priv == odabranakartica1) {
            
            priv--;
          }
        }
      if (priv>=0) {
        Pokaznik(priv);
      }
    }

  
  kartica1 = true;
  delay(1000);
}


void OdabirKartice2() {

  if (ledpomakdis == 0) {
    if (ledpomakx == 1) {
      if (ledpomaky == 1) {
        odabranakartica2 = 0;
      }
      if (ledpomaky == 5) {
        odabranakartica2 = 1;
      }
    }

    if (ledpomakx == 5) {
      if (ledpomaky == 1) {
        odabranakartica2 = 2;
      }
      if (ledpomaky == 5) {
        odabranakartica2 = 3;
      }
    }
  }

  if (ledpomakdis == 1) {
    if (ledpomakx == 1) {
      if (ledpomaky == 1) {
        odabranakartica2 = 4;
      }
      if (ledpomaky == 5) {
        odabranakartica2 = 5;
      }
    }

    if (ledpomakx == 5) {
      if (ledpomaky == 1) {
        odabranakartica2 = 6;
      }
      if (ledpomaky == 5) {
        odabranakartica2 = 7;
      }
    }
  }

  if (ledpomakdis == 2) {
    if (ledpomakx == 1) {
      if (ledpomaky == 1) {
        odabranakartica2 = 8;
      }
      if (ledpomaky == 5) {
        odabranakartica2 = 9;
      }
    }

    if (ledpomakx == 5) {
      if (ledpomaky == 1) {
        odabranakartica2 = 10;
      }
      if (ledpomaky == 5) {
        odabranakartica2 = 11;
      }
    }
  }

  if (ledpomakdis == 3) {
    if (ledpomakx == 1) {
      if (ledpomaky == 1) {
        odabranakartica2 = 12;
      }
      if (ledpomaky == 5) {
        odabranakartica2 = 13;
      }
    }

    if (ledpomakx == 5) {
      if (ledpomaky == 1) {
        odabranakartica2 = 14;
      }
      if (ledpomaky == 5) {
        odabranakartica2 = 15;
      }
    }
  }

  picindex2 = kartice[odabranakartica2];

  switch (picindex2) {
    case 0:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 1:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, false);
      break;

    case 2:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, false);
      break;

    case 3:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 4:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 5:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 6:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, false);
      break;

    case 7:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;

    case 8:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, false);
      break;

    case 9:
      led.setLed(ledpomakdis, ledpomaky, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, false);
      led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
      led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
      break;
  }



  kartica2 = true;
  if (kartica1 == true && kartica2 == true) {
  ProvjeraPara();
  delay(370);
  }
}

void ProvjeraPara() {
  if (picindex1 == picindex2) {
    rezultat++;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Rezultat: ");
    lcd.setCursor(0, 1);
    lcd.print(rezultat);
    kartica1 = false;
    kartica2 = false;
    brojac++;
    //prva pogodjena kartica se sprema u matricu koordinate
    koordinate[brojac][0] = koordinate[0][0];
    koordinate[brojac][1] = koordinate[0][1];
    koordinate[brojac][2] = koordinate[0][2];
    koordinateindex[brojac-1] = odabranakartica1;
    Sort();
    brojac++;
    //druga pogodjena kartica se sprema u matricu koordinate
    koordinate[brojac][0] = ledpomakdis;
    koordinate[brojac][1] = ledpomakx;
    koordinate[brojac][2] = ledpomaky;
    koordinateindex[brojac-1] = odabranakartica2;
    Sort();

    int priv=0;
    for (int i=0; i<brojac; i++) {
      if (priv == koordinateindex[i]) {
        priv++;
     }
    }
   Pokaznik(priv);

    if (rezultat == tezina / 2) {
      delay(1000);
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Igra je gotova!");
      lcd.setCursor(0, 1);
      lcd.print("Cestitke!");
      delay(3000);
      setup();
      pomak=0;
      tezinaflag=true;
      ledpomakx=1;
      ledpomaky=1;
      ledpomakdis=0;
      igraflag=false;
      rezultat=0;
      brojac=0;
      karticaflag=false;

     
    }
  }
  else {
    kartica1 = false;
    kartica2 = false;
    delay(1700);
    led.setLed(ledpomakdis, ledpomaky, ledpomakx, true);
    led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx, true);
    led.setLed(ledpomakdis, ledpomaky, ledpomakx + 1, true);
    led.setLed(ledpomakdis, ledpomaky + 1, ledpomakx + 1, true);
    led.setLed(koordinate[0][0], koordinate[0][2], koordinate[0][1], true);
    led.setLed(koordinate[0][0], koordinate[0][2] + 1, koordinate[0][1], true);
    led.setLed(koordinate[0][0], koordinate[0][2], koordinate[0][1] + 1, true);
    led.setLed(koordinate[0][0], koordinate[0][2] + 1, koordinate[0][1] + 1, true);
  }
  koordinate[0][0] = 20;
  koordinate[0][1] = 20;
  koordinate[0][2] = 20;
  koordindex = 20;
  odabranakartica1=20;
  odabranakartica2=21;

}

void Sort() { //selection sort
  int mjesto, swap;

  for (int i=0; i<brojac; i++) {
    mjesto=i;
    for (int j=i+1; j<brojac; j++) {
      if (koordinateindex[mjesto] > koordinateindex[j]) {
        mjesto=j;
      }
      if (mjesto != i) {
        swap=koordinateindex[i];
        koordinateindex[i]=koordinateindex[mjesto];
        koordinateindex[mjesto]=swap;
      }
    }
  }
}

void Pokaznik(int priv) {
  for (int i=0; i<brojac; i++) {
    if (priv == koordinateindex[i]) {
      priv++;
    }
  }
    
  switch(priv) {
    case 0:
      ledpomakdis=0;
      ledpomakx=1;
      ledpomaky=1;
      break;
    case 1:
      ledpomakdis=0;
      ledpomakx=1;
      ledpomaky=5;
      break;
    case 2:
      ledpomakdis=0;
      ledpomakx=5;
      ledpomaky=1;
      break;
    case 3:
      ledpomakdis=0;
      ledpomakx=5;
      ledpomaky=5;
      break;
    case 4:
      ledpomakdis=1;
      ledpomakx=1;
      ledpomaky=1;
      break;
    case 5:
      ledpomakdis=1;
      ledpomakx=1;
      ledpomaky=5;
      break;
    case 6:
      ledpomakdis=1;
      ledpomakx=5;
      ledpomaky=1;
      break;
    case 7:
      ledpomakdis=1;
      ledpomakx=5;
      ledpomaky=5;
      break;
    case 8:
      ledpomakdis=2;
      ledpomakx=1;
      ledpomaky=1;
      break;
    case 9:
      ledpomakdis=2;
      ledpomakx=1;
      ledpomaky=5;
      break;
    case 10:
      ledpomakdis=2;
      ledpomakx=5;
      ledpomaky=1;
      break;
    case 11:
      ledpomakdis=2;
      ledpomakx=5;
      ledpomaky=5;
      break;
    case 12:
      ledpomakdis=3;
      ledpomakx=1;
      ledpomaky=1;
      break;
    case 13:
      ledpomakdis=3;
      ledpomakx=1;
      ledpomaky=5;
      break;
    case 14:
      ledpomakdis=3;
      ledpomakx=5;
      ledpomaky=1;
      break;
    case 15:
      ledpomakdis=3;
      ledpomakx=5;
      ledpomaky=5;
      break;
  }
}

